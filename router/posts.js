const { promise } = require('bcrypt/promises');
const express = require('express');
const router = express.Router();

const postcontroller = require('../controller/controller')

const use=fn=>(req, res, next)=>
Promise.resolve(fn(req, res, next)).catch(next);


router.post('/', use(postcontroller.createPost));

router.get('/:id', use(postcontroller.getPost));

router.put('/:id', use(postcontroller.updatePost));

router.delete('/:id', use(postcontroller.deletePost));

module.exports = router;
