

const { promise } = require('bcrypt/promises');
const express = require('express');
const router = express.Router();
const {checkToken} = require(".././auth/verifyToken");

const postcontroller = require('../controller/controller')

const use=fn=>(req, res, next)=>
Promise.resolve(fn(req, res, next)).catch(next);


router.post('/register', use(postcontroller.register));

router.post('/login', use(postcontroller.login));


router.post('/verify', use(postcontroller.detail));




module.exports = router;
