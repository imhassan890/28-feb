const Post = require("../models/Post");
const User = require("../models/User");

const jwt = require('jsonwebtoken')

const Joi = require("joi");
const app = require('express')

const bcrypt = require("bcrypt");
const dotenv = require("dotenv");
const { append } = require("express/lib/response");

dotenv.config();
const userdata = {
//CREATE POST
async createPost (req, res) {
  // const schema = { 
  //   title:Joi.string().min(3).required(),
  //   desc:Joi.string().min(3).required()
  // };
  // // const result = Joi.string().validate(req.body,schema);
  //  const result = Joi.string().validate(req.body,schema);

  // if (result.error) {
  //   res.status(400).send(result.error.details[0].message);
  //   return;
  //  }
  const newPost = new Post(req.body);
  try {
    const savedPost = await newPost.save();
    res.status(200).json(savedPost);
  } catch (err) {
    res.status(500).json(err);
  }
},

//UPDATE POST
async  updatePost (req, res) {
 
  // const schema = {

  //   title:Joi.string().min(3).required(),
  //   desc:Joi.string().min(3).required()
  // };

  // const result = Joi.string().validate(req.body,schema);

  // if (result.error) {
  //   res.status(400).send(result.error.details[0].message);
  //   return;
  //  }

  try {
    const post = await Post.findById(req.params.id);
    if (post.username === req.body.username) {
      try {
        const updatedPost = await Post.findByIdAndUpdate(
          req.params.id,
          {
            $set: req.body,
          },
          { new: true }
        );
        res.status(200).json(updatedPost);
      } catch (err) {
        res.status(500).json(err);
      }
    } else {
      res.status(401).json("You can update only your post!");
    }
  } catch (err) {
    res.status(500).json(err);
  }
  },

  //DELETE POST
 async deletePost (req, res) {
  try {
    const post = await Post.findById(req.params.id);
    if (post.username === req.body.username) {
      try {
        await post.delete();
        res.status(200).json("Post has been deleted...");
      } catch (err) {
        res.status(500).json(err);
      }
    } else {
      res.status(401).json("You can delete only your post!");
    }
  } catch (err) {
    res.status(500).json(err);
  }
  },

  //GET POST
async getPost(req, res){
    try {
      const post = await Post.findById(req.params.id);
      res.status(200).json(post);
    } catch (err) {
      res.status(500).json(err);
    }
  },

//REGISTER
async register (req, res) {
  try {
    const salt = await bcrypt.genSalt(10);
    const hashedPass = await bcrypt.hash(req.body.password, salt);
    const newUser = new User({
      username: req.body.username,
      email: req.body.email,
      password: hashedPass,
    });
    const user = await newUser.save();
    res.status(200).json(user);
  } catch (err) {
    res.status(500).json(err);
  }
},

//LOGIN
async login (req, res) {
  try {
    const user = await User.findOne({ username: req.body.username });
    !user && res.status(400).json("Wrong credentials!");

    const validated = await bcrypt.compare(req.body.password, user.password);
    if(validated){
        
    const accessToken=jwt.sign({user},process.env.ACCESS_TOKEN_SECRET,{ expiresIn: '1h' });
    res.json({ success: true, message: "Login Successfully", accessToken })
    }

    else
       res.status(400).json("Wrong credentials!");

  } catch (err) {
    res.status(500).json(err);
  }
},
async detail(req, res) {

  const userdata = await User.findOne({ username: req.body.username });
  res.json({
      success: true,
      message: "Token is verfiy",
      userdata
  })
}
};

function authenticateToken(req, res,next){
  const authHeader= req.headers['authorization']
  const token = authHeader && authHeader.split(' ')[1]
  if(token == null) return res.sendStatus(401)
  jwt.verify(token,process.env.ACCESS_TOKEN_SECRET,(err,user)=>{
    if(err) return res.sendStatus(403)
    req.user = user
    next()
  })
}

module.exports = userdata ; 