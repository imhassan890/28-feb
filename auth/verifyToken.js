const dotenv = require('dotenv');
dotenv.config();
const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {

    let token = req.get('authorization');
    if (token) {
        token = token.slice(7);
        jwt.verify(token, process.env.SERCET_KEY, (err, decoded) => {
            if (err) {
                res.json({ success: false, message: "Invalid token" })
            } else {
                req.user = decoded
                next();
            }
        })
    } else {
        res.json({
            success: false,
            message: 'Access denied unauthorizate user'
        })

    }
}
